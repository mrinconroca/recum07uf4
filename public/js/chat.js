const e = require("express");
const { Socket } = require("socket.io");

$(document).ready(function () {
  //Inicializa socket con IO
  const socket = io();
  //Cuando cambia el select redirigimos a la URL del chat
  $('#selectRoom').on("change", () => {
    var sala = $(this).find("option:selected").val();
    window.location.href = "/chat/" + sala;
    console.log(window.location.href);
  });
  //Accion cuando el usuario envia mensaje con submit
  $("#chat").submit(function (e) {
    e.preventDefault();
    var msg = $("#msg").val();
    $("#chatBox").append(`<p>${msg}<p>`);
    var enviar = { sala: "sala" };
    socket.emit("mensaje", enviar);
  });

  //Acciones a realizar cuando se detecta actividad en el canal newMsg
  $("#chat").on("mensaje", () => {
    $("#chatBox").append(`<p>${msg}<p>`);
  })
});