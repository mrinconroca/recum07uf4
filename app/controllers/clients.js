var mongoose = require("mongoose"),
  Model = require("../models/client");

exports.getAll = async (req, res, next) => {
  var res = await Model.find({})
  return res;
};

exports.find = async (req, res, next) => {

};

exports.delete = async (req, res, next) => {
  var incDel = {
    _id: req.params.id,
  };
  Model.deleteOne(incDel, function (err) {
    if (err) return handleError(err);
  });
};

exports.add = async (req, res, next) => {
  var el = {
    id: req.params.id,
    name: req.params.name,
    surname: req.params.surname,
  };
  var newForm = new Model(el);
  newForm.save((err, res) => {
    if (err) console.log(err);
    console.log("Cliente guardado en la DB");
    return res;
  });
};