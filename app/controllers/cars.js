var mongoose = require("mongoose"),
  Model = require("../models/car");

exports.getAll = async (req, res, next) => {
  var res = await Model.find({})
  return res;
};

exports.find = async (req, res, next) => {
  var res = await Model.find({ brand: "BMW" })
  return res;
};

exports.delete = async (req, res, next) => {
  var incDel = {
    _id: req.params.id,
  };
  Model.deleteOne(incDel, function (err) {
    if (err) return handleError(err);
  });
};

exports.add = async (req, res, next) => {
  var el = {
    id: req.params.id,
    brand: req.params.brand,
    model: req.params.model,
    price: req.params.price,
  };
  var newForm = new Model(el);
  newForm.save((err, res) => {
    if (err) console.log(err);
    console.log("Coche guardado en la DB");
    return res;
  });
};