var express = require("express"),
  path = require("path"),
  router = express.Router(),
  ctrlDir = "/app/app/controllers",
  carsCtrl = require(path.join(ctrlDir, "cars")),
  rentsCtrl = require(path.join(ctrlDir, "rents")),
  clientsCtrl = require(path.join(ctrlDir, "clients"));


//router.route("/chat/:id");
//router.route("/rent/new");
//router.route("/rent/list");

// Cargar el formulario
router.route('/rent/new').get(async (req, res, next) => {
  var result = await rentsCtrl.getAll();
  var carList = await carsCtrl.getAll();
  var clientsList = await clientsCtrl.getAll();
  console.log(result);
  res.render("form", { result: result, cars: carList, clients: clientsList });
});

//Guardar datos del formulario
router.post('/rent/save', async (req, res) => {
  rentsCtrl.add(req, res);
  res.redirect("/rent/list");
});

// Cargar página de la tabla 
router.route('/rent/list').get(async (req, res, next) => {
  var result = await rentsCtrl.getAll();
  console.log(result);
  res.render("list", { rents: result });
});

// Cargar el chat
router.route('/chat/:id').get(async (req, res, next) => {
  res.render("chat");
});

module.exports = router;
